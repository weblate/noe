.DEFAULT_GOAL := full-start

### DOCKER_COMPOSE VARIABLES ###

# The base config is in `docker-compose.yml`
DOCKER_COMPOSE=docker-compose -f docker-compose.yml
# In a local environment, add the `docker-compose.local.yml` configuration and env variables from `.env.local` file
DOCKER_COMPOSE_LOCAL=$(DOCKER_COMPOSE) -f docker-compose.local.yml --env-file .env.local
# For local development, add the `docker-compose.local.dev.yml` configuration to `docker-compose.local.yml`
DOCKER_COMPOSE_DEV=$(DOCKER_COMPOSE_LOCAL) -f docker-compose.local.dev.yml
# For tests, add the `docker-compose.local.test.yml` configuration to `docker-compose.local.yml`
DOCKER_COMPOSE_TEST=$(DOCKER_COMPOSE_LOCAL) -f docker-compose.local.test.yml
# To deploy in production, add config from `docker-compose.prod.yml` and env variables from `.env.prod` file
DOCKER_COMPOSE_PROD=$(DOCKER_COMPOSE) -f docker-compose.prod.yml --env-file .env.prod

# Listing of different services of the app
SERVICES=api inscription-front orga-front ia-back website


############################
########### LOGS ###########
############################

log:
	docker-compose logs -f --tail=10000 api
log-back:
	docker-compose logs -f --tail=10000 api ia-back
log-front:
	docker-compose logs -f --tail=10000 orga-front inscription-front
log-all:
	docker-compose logs -f --tail=10000 api orga-front inscription-front ia-back website

log-list-files:
	ls -l api/logs


############################
###### INITIALIZATION ######
############################

### DEVELOPMENT ###

init: install-deps-global-project build install-deps-full

install-deps-global-project:
	npm install

install-deps-full: install-deps-api install-deps-inscription-front install-deps-orga-front install-deps-ia-back install-deps-website
install-deps-api:
	$(DOCKER_COMPOSE) run --no-deps api npm install --omit=optional
install-deps-ia-back:
	$(DOCKER_COMPOSE) run --no-deps ia-back npm install --omit=optional
install-deps-orga-front:
	$(DOCKER_COMPOSE) run --no-deps orga-front npm install --legacy-peer-deps
install-deps-inscription-front:
	$(DOCKER_COMPOSE) run --no-deps inscription-front npm install --legacy-peer-deps
install-deps-website:
	$(DOCKER_COMPOSE) run --no-deps website npm install


############################
#### DEVELOPMENT START #####
############################

# Start all containers
full-start:
	$(DOCKER_COMPOSE_DEV) up -d api inscription-front orga-front
full-start-optional:
	$(DOCKER_COMPOSE_DEV) up -d

# In dev, allow soft restart without recreating the container for convenience
start-api:
	$(DOCKER_COMPOSE_DEV) up -d api
start-ia-back:
	$(DOCKER_COMPOSE_DEV) up -d ia-back
start-orga-front:
	$(DOCKER_COMPOSE_DEV) up -d orga-front
start-inscription-front:
	$(DOCKER_COMPOSE_DEV) up -d inscription-front
start-website:
	$(DOCKER_COMPOSE_DEV) up -d website

# Local production build for frontends
start-build-orga-front: stop-orga-front
	$(DOCKER_COMPOSE_LOCAL) up -d orga-front
start-build-inscription-front: stop-inscription-front
	$(DOCKER_COMPOSE_LOCAL) up -d inscription-front
start-build-website: stop-website
	$(DOCKER_COMPOSE_LOCAL) up -d website

# Stop containers
full-stop:
	$(DOCKER_COMPOSE_LOCAL) down
stop-api:
	$(DOCKER_COMPOSE) stop api
stop-ia-back:
	$(DOCKER_COMPOSE) stop ia-back
stop-orga-front:
	$(DOCKER_COMPOSE) stop orga-front
stop-inscription-front:
	$(DOCKER_COMPOSE) stop inscription-front
stop-website:
	$(DOCKER_COMPOSE) stop website

# Force restart for containers
full-restart: full-stop full-start
restart-api: stop-api start-api
restart-ia-back: stop-ia-back start-ia-back
restart-orga-front: stop-orga-front start-orga-front
restart-inscription-front: stop-inscription-front start-inscription-front
restart-website: stop-website start-website

############################
#### PRODUCTION  START #####
############################

# Start all containers
full-start-prod:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate api inscription-front orga-front traefik
full-start-prod-optional:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate

# Stop containers
full-stop-prod:
	$(DOCKER_COMPOSE_PROD) down
stop-prod-api: stop-api
stop-prod-ia-back: stop-ia-back
stop-prod-orga-front: stop-orga-front
stop-prod-inscription-front: stop-inscription-front
stop-prod-website: stop-website

# Force restart for containers (no soft restart in production, we use --force-recreate)
full-restart-prod: full-stop full-start-prod
restart-prod-api:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate api
restart-prod-ia-back:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate ia-back
restart-prod-orga-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate orga-front
restart-prod-inscription-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate inscription-front
restart-prod-website:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate website


############################
### LOCAL NETWORK START ####
############################

start-local-network:
	$(DOCKER_COMPOSE_LOCAL) up -d


########################
######## OTHERS ########
########################

### DOCKER ###

# Launch the building process for all the containers
build:
	$(DOCKER_COMPOSE) build

# Clean all the build and temporary assets in all folders
clean:
	for service in $(SERVICES) ; do \
		$(DOCKER_COMPOSE) run --no-deps $$service npm run clean ; \
	done

# Kill and remove all docker containers
kill:
	$(DOCKER_COMPOSE_LOCAL) kill
	$(DOCKER_COMPOSE_LOCAL) rm -v

### WEBSITE UTILS

write-translations-website:
	npm run write-translations --prefix website -- --locale fr --messagePrefix 'TODO '
	npm run write-translations --prefix website -- --locale en --messagePrefix 'TODO '

### LINTING & FORMATTING ###

# Make a manual `npm run prettier` in all subdirectories
prettier:
	npm run prettier

# Make a manual `npm run prettier-check` in all subdirectories
prettier-check:
	npm run prettier-check

# Make a manual `npm run lint` in all subdirectories
lint:
	for service in $(SERVICES) ; do \
		npm run lint --prefix ./$$service ; \
	done

# Make a manual `npm run lint-show` in all subdirectories
lint-show:
	for service in $(SERVICES) ; do \
		npm run lint-show --prefix ./$$service || exit 1 ; \
	done

#### TESTS ####

# For tests we currently only need fuseki
test:
	$(DOCKER_COMPOSE_TEST) up api --exit-code-from=api
	$(DOCKER_COMPOSE_TEST) up ia-back --exit-code-from=ia-back

