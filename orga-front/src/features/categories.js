import {createSlice} from "@reduxjs/toolkit";
import {
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {activitiesActions} from "./activities";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";

export const categoriesSlice = createSlice({
  name: "categories",
  initialState: {
    list: [],
    init: false,
    editing: {},
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    changeEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        ...action.payload,
      };
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "categories",
      projectId,
      state.categories.init,
      () => dispatch(categoriesActions.initContext(projectId)),
      (data) => dispatch(categoriesActions.initList({list: data, project: projectId}))
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "categories",
      entityId,
      projectId,
      state.categories.editing,
      () => dispatch(categoriesActions.setEditing({_id: "new"})),
      (data) => dispatch(categoriesActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(
            categoriesActions.setEditing(state.categories.list.find((s) => s._id === entityId))
          ),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.categories.editing;

    return persistEntityInBackend(
      "categories",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(categoriesActions.addToList(data)),
      (data) => {
        dispatch(categoriesActions.updateInList(data));
        // Update all dependencies next time
        dispatch(sessionsActions.initContext(false));
        dispatch(activitiesActions.initContext(false));
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "categories",
      entityId,
      projectId,
      state.categories.list,
      (newCategoriesList) =>
        dispatch(
          categoriesActions.initList({
            list: newCategoriesList,
            project: state.categories.init,
          })
        )
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "categories",
        `${entityId}/history`,
        projectId,
        state.categories.editing,
        null,
        (data) => dispatch(categoriesActions.changeEditing({history: data}))
      ));
  },
};

export const categoriesSelectors = {
  selectEditing: (state) => state.categories.editing,
  selectList: (state) => state.categories.list,
};

export const categoriesReducer = categoriesSlice.reducer;

export const categoriesActions = {
  ...categoriesSlice.actions,
  ...asyncActions,
};
