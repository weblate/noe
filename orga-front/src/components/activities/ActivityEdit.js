import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {Alert, Button, Collapse, Form, Modal} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {EditPage, useNewElementModal} from "../common/EditPage";
import {listRenderer} from "../../helpers/listUtilities";
import {
  addKeyToItemsOfList,
  columnsPlaces,
  columnsStewards,
  dataToFields,
  fieldToData,
  generateSessionsColumns,
} from "../../helpers/tableUtilities";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {navigate} from "@reach/router";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {CategoryEdit, ColorDot} from "../categories/CategoryEdit";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";
import {StewardEdit} from "../stewards/StewardEdit";
import {PlaceEdit} from "../places/PlaceEdit";
import {Trans, useTranslation} from "react-i18next";

export function ActivityEdit({id, location, asModal, modalVisible, setModalVisible}) {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const activity = useSelector(activitiesSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const dispatch = useDispatch();
  const [showModalSteward, setShowModalSteward] = useState(false);
  const [showModalPlace, setShowModalPlace] = useState(false);
  const [showModalSlot, setShowModalSlot] = useState(false);
  const [setShowNewCategoryModal, NewCategoryModal] = useNewElementModal(CategoryEdit);
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const [setShowNewPlaceModal, NewPlaceModal] = useNewElementModal(PlaceEdit);
  const [addingSteward, setAddingSteward] = useState([]);
  const [addingPlace, setAddingPlace] = useState([]);
  const [currentSlot, setCurrentSlot] = useState();
  const [slotForm] = Form.useForm();
  const [isModified, setIsModified] = useState(false);
  const [activitySessionsHelpCollapseKeys, setActivitySessionsHelpCollapseKeys] =
    useLocalStorageState("activitySessionsHelpCollapseKeys", true);

  const activitySessions = sessions.filter((s) => s.activity?._id === activity._id);

  const insufficientNumberOfStewards =
    activity && activity.stewards?.length < activity.minNumberOfStewards;

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    activitiesActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(categoriesActions.loadList());
      dispatch(sessionsActions.loadList());
      dispatch(activitiesActions.loadList()); // For tags
    },
    clonedElement
  );

  const stewardsAddable =
    activity.stewards === undefined
      ? []
      : stewards.filter((s) => activity.stewards.filter((as) => as._id === s._id).length === 0);
  const placesAddable =
    activity.places === undefined
      ? []
      : places.filter((p) => activity.places.filter((ap) => ap._id === p._id).length === 0);

  const activitySlotsTable = addKeyToItemsOfList(activity.slots);
  const columnsSlots = [{dataIndex: "duration", render: listRenderer.durationFormat}];

  const rowSelectionSteward = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingSteward(selectedRowObject);
    },
  };
  const rowSelectionPlace = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingPlace(selectedRowObject);
    },
  };

  const addStewards = () => {
    dispatch(activitiesActions.addStewardsToEditing(addingSteward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const addPlaces = () => {
    dispatch(activitiesActions.addPlacesToEditing(addingPlace));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSteward = (steward) => {
    dispatch(activitiesActions.removeStewardToEditing(steward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const removePlace = (place) => {
    dispatch(activitiesActions.removePlaceToEditing(place));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSlot = (slot) => {
    let slots = [...activitySlotsTable.filter((r) => r.key !== slot.key)];
    dispatch(activitiesActions.changeSlotsToEditing(slots));
    setIsModified(true);
  };

  const onChangeSlot = (changedFields, allFields) =>
    setCurrentSlot({...currentSlot, ...fieldToData(allFields)});

  const onValidateSlot = () => {
    slotForm.validateFields().then(() => {
      let slots;
      if (currentSlot.key === undefined) {
        slots = [...activitySlotsTable, currentSlot];
      } else {
        slots = [...activitySlotsTable];
        slots[slots.findIndex((r) => r.key === currentSlot.key)] = currentSlot;
      }
      dispatch(activitiesActions.changeSlotsToEditing(slots));

      setCurrentSlot(undefined);
      slotForm.resetFields(["duration"]);
      setShowModalSlot(false);
      setIsModified(true);
    });
  };

  const activityFieldToData = (fields) => {
    let data = fieldToData(fields);
    const categoryIndex = categories.map((d) => d._id).indexOf(data.category);
    if (categoryIndex >= 0) {
      data.category = {...categories[categoryIndex]};
    }
    return data;
  };

  return (
    <>
      <EditPage
        i18nNs="activities"
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        deletable
        elementsActions={activitiesActions}
        record={activity}
        initialValues={{...activity, category: activity.category?._id}}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["category"]}
        fieldToData={activityFieldToData}
        groupEditing={groupEditing}>
        <CardElement>
          <div className="container-grid three-per-row">
            <InputElement.Text
              i18nNs="activities"
              name="name"
              rules={[
                {required: true},
                {
                  max: 75,
                  message: t("activities:schema.name.tooLong"),
                  warningOnly: true,
                },
              ]}
            />

            <InputElement.WithEntityLinks
              endpoint="categories"
              entity={activity.category}
              createButtonText={t("categories:label", {context: "createNew"})}
              setShowNewEntityModal={setShowNewCategoryModal}>
              <InputElement.Select
                label={t("categories:label")}
                name="category"
                placeholder={t("categories:label").toLowerCase()}
                rules={[{required: true}]}
                options={categories.map((m) => ({
                  value: m._id,
                  label: (
                    <div className="containerH" style={{alignItems: "center"}}>
                      <ColorDot
                        color={m.color}
                        size={15}
                        style={{marginBottom: 1, marginRight: 8}}
                      />
                      {m.name}
                    </div>
                  ),
                }))}
                showSearch
              />
            </InputElement.WithEntityLinks>

            <InputElement.TagsSelect
              i18nNs="activities"
              name="secondaryCategories"
              elementsSelectors={activitiesSelectors}
            />

            <InputElement.Switch i18nNs="activities" name="allowSameTime" />

            <InputElement.Number
              i18nNs="activities"
              name="volunteeringCoefficient"
              step={0.1}
              min={0}
              max={5}
            />

            <InputElement.Number
              i18nNs="activities"
              name="stewardVolunteeringCoefficient"
              step={0.1}
              defaultValue={1.5}
              min={0}
              max={5}
            />

            <InputElement.Number i18nNs="activities" name="maxNumberOfParticipants" min={0} />
          </div>
        </CardElement>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <InputElement.Editor i18nNs="activities" name="description" />

            <InputElement.TextArea
              i18nNs="activities"
              name="summary"
              rules={[
                {max: 250, message: <Trans i18nKey="schema.summary.tooLong" ns="activities" />},
              ]}
            />

            <InputElement.Editor i18nNs="activities" name="notes" />

            <InputElement.TagsSelect
              i18nNs="activities"
              name="tags"
              elementsSelectors={activitiesSelectors}
            />
          </div>
        </CardElement>

        {currentProject.useAI && (
          <CardElement title={t("activities:createWithIA")}>
            <div className="container-grid three-per-row">
              <InputElement.Number
                i18nNs="activities"
                name="numberOfSessions"
                min={1}
                rules={[{required: true}]}
              />
              <InputElement.Number i18nNs="activities" name="minNumberOfStewards" min={0} />
              <InputElement.Switch i18nNs="activities" name="nonBlockingActivity" />
            </div>
          </CardElement>
        )}

        <Collapse
          style={{marginBottom: 26}}
          onChange={setActivitySessionsHelpCollapseKeys}
          activeKey={currentProject.useAI ? ["sessions-help"] : activitySessionsHelpCollapseKeys}>
          <Collapse.Panel
            header={t("activities:helpForSessionsCreation.title")}
            key="sessions-help">
            <p>
              {t("activities:helpForSessionsCreation.information")}
              {currentProject.useAI &&
                " " + t("activities:helpForSessionsCreation.informationUseAI")}
            </p>

            <div
              style={{marginBottom: -26}}
              className={
                "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
              }>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title={t("activities:schema.slots.label")}
                  buttonTitle={t("activities:schema.slots.buttonTitle")}
                  tooltip={t("activities:schema.slots.tooltip")}
                  onClickButton={() => {
                    setCurrentSlot(undefined);
                    setShowModalSlot(true);
                  }}
                  onEdit={(record) => {
                    setCurrentSlot(record);
                    setShowModalSlot(true);
                  }}
                  onDelete={removeSlot}
                  columns={columnsSlots}
                  dataSource={activitySlotsTable}
                />
              </div>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title={t("activities:schema.stewards.label")}
                  buttonTitle={t("activities:schema.stewards.buttonTitle")}
                  subtitle={
                    insufficientNumberOfStewards && (
                      <Alert
                        message={t("activities:schema.stewards.numberOfStewardsIsBelowMinimum")}
                        type="warning"
                        showIcon
                      />
                    )
                  }
                  tooltip={t("activities:schema.stewards.tooltip")}
                  onClickButton={() => setShowModalSteward(true)}
                  onDelete={removeSteward}
                  columns={columnsStewards}
                  navigableRootPath="../stewards"
                  dataSource={activity.stewards}
                />
              </div>
              {currentProject.usePlaces && (
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <TableElement.WithTitle
                    title={t("activities:schema.places.label")}
                    buttonTitle={t("activities:schema.places.buttonTitle")}
                    tooltip={t("activities:schema.places.tooltip")}
                    onClickButton={() => setShowModalPlace(true)}
                    onDelete={removePlace}
                    columns={columnsPlaces}
                    navigableRootPath="../places"
                    dataSource={activity.places}
                  />
                </div>
              )}
            </div>
          </Collapse.Panel>
        </Collapse>

        {activity._id !== "new" && (
          <TableElement.WithTitle
            title={t("activities:schema.sessions.label")}
            showHeader
            buttonTitle={t("activities:schema.sessions.buttonTitle")}
            onClickButton={() => {
              dispatch(
                sessionsActions.changeEditing({
                  _id: "new",
                  activity,
                  places: [],
                  stewards: [],
                  slots: [],
                })
              );
              navigate("../sessions/new");
            }}
            navigableRootPath="../sessions"
            columns={generateSessionsColumns("../..", currentProject.usePlaces, true, true)}
            dataSource={activitySessions}
          />
        )}
      </EditPage>

      {/* MODALS */}

      <Modal
        title={t("activities:schema.slots.editModal.title")}
        visible={showModalSlot}
        onOk={onValidateSlot}
        onCancel={() => {
          slotForm.resetFields(["duration"]);
          setShowModalSlot(false);
          setCurrentSlot(undefined);
        }}>
        <FormElement
          form={slotForm}
          fields={dataToFields(currentSlot)}
          onFieldsChange={onChangeSlot}
          validateAction={onValidateSlot}>
          <div className="container-grid">
            <InputElement.Number
              autoFocus={true}
              label={t("activities:schema.slots.editModal.duration.label")}
              placeholder={t("activities:schema.slots.editModal.duration.placeholder")}
              name="duration"
              min={1}
              rules={[{required: true}, {type: "number"}]}
            />
            {currentSlot?.duration && (
              <div className="fade-in">
                {t("activities:schema.slots.editModal.duration.whichRepresents", {
                  duration: listRenderer.durationFormat(currentSlot.duration),
                })}
              </div>
            )}
          </div>
        </FormElement>
      </Modal>

      <LayoutElement.ElementSelectionModal
        title={t("activities:schema.slots.editModal.stewards.label")}
        visible={showModalSteward}
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            {t("activities:schema.slots.editModal.stewards.buttonTitle")}
          </Button>
        }
        onOk={() => {
          addStewards();
          setShowModalSteward(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalSteward(false)}
        rowSelection={rowSelectionSteward}
        searchInFields={["firstName", "lastName"]}
        selectedRowKeys={addingSteward}
        setSelectedRowKeys={setAddingSteward}
        columns={columnsStewards}
        dataSource={stewardsAddable}
      />

      <LayoutElement.ElementSelectionModal
        title={t("activities:schema.slots.editModal.places.label")}
        visible={showModalPlace}
        customButtons={
          <Button type="link" onClick={() => setShowNewPlaceModal(true)}>
            {t("activities:schema.slots.editModal.places.buttonTitle")}
          </Button>
        }
        onOk={() => {
          addPlaces();
          setShowModalPlace(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalPlace(false)}
        rowSelection={rowSelectionPlace}
        searchInFields={["name"]}
        selectedRowKeys={addingPlace}
        setSelectedRowKeys={setAddingPlace}
        columns={columnsPlaces}
        dataSource={placesAddable}
      />

      <NewCategoryModal />
      <NewStewardModal />
      {currentProject.usePlaces && <NewPlaceModal />}
    </>
  );
}
