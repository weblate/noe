import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {SwatchesPicker} from "react-color";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {Alert, Tag} from "antd";
import {CardElement} from "../common/LayoutElement";
import {useTranslation} from "react-i18next";

export const ColorDot = ({size = 26, color, style}) => (
  <div
    className="containerH"
    style={{
      height: size,
      width: size,
      backgroundColor: color,
      borderRadius: "50%",
      ...style,
    }}
  />
);

export function CategoryEdit({id, location, asModal, modalVisible, setModalVisible}) {
  const {t} = useTranslation();
  const [isModified, setIsModified] = useState(false);
  const category = useSelector(categoriesSelectors.selectEditing);
  const categories = useSelector(categoriesSelectors.selectList);
  const dispatch = useDispatch();

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    categoriesActions,
    id,
    () => {
      dispatch(categoriesActions.loadList());
    },
    clonedElement
  );

  const onChangeColor = (color) => {
    dispatch(categoriesActions.changeEditing({color: color.hex}));
    setIsModified(true);
  };

  return (
    <EditPage
      i18nNs="categories"
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      deletable
      elementsActions={categoriesActions}
      record={category}
      forceModifButtonActivation={isModified}
      initialValues={category}
      groupEditing={groupEditing}
      fieldsThatNeedReduxUpdate={["name"]}>
      <div className="container-grid two-per-row">
        <CardElement>
          <div className="container-grid">
            <InputElement.Text
              i18nNs="categories"
              name="name"
              rules={[
                {required: true},
                {
                  validator: (_, value) => {
                    return 30 <= value.length && value.length <= 45
                      ? Promise.reject(new Error(t("categories:schema.name.tooLong")))
                      : Promise.resolve();
                  },
                  warningOnly: true,
                },
                {max: 45, message: t("categories:schema.name.reallyTooLong")},
              ]}
            />

            <InputElement.TextArea i18nNs="categories" name="summary" />
          </div>
        </CardElement>

        <CardElement>
          <InputElement.Custom
            label={t("categories:schema.color.label")}
            name="colorField"
            rules={[
              {
                validator: () =>
                  category.color
                    ? Promise.resolve()
                    : Promise.reject(new Error(t("categories:schema.color.pleaseChooseAColor"))),
              },
            ]}>
            <Alert
              type="info"
              style={{marginBottom: 15}}
              message={t("categories:schema.color.bewareOfColorsThatAreTooLight")}
            />
            <SwatchesPicker
              height="auto"
              width="100%"
              color={category.color}
              onChangeComplete={onChangeColor}
            />
          </InputElement.Custom>

          {categories.length > 0 && (
            <div style={{marginTop: 26}}>
              <p>{t("categories:schema.color.lookHowThisColorFitsInThePalette")}</p>
              <div className="containerH" style={{gap: 8, flexWrap: "wrap"}}>
                {categories
                  .filter((c) => category._id !== c._id)
                  .map((c) => (
                    <ColorDot color={c.color} />
                  ))}
                <div className="containerH" style={{alignItems: "center"}}>
                  <ColorDot
                    color={category.color}
                    style={{marginRight: 8, transition: "all .3s"}}
                  />
                  <Tag
                    style={{textOverflow: "ellipsis", overflow: "hidden", maxWidth: 125}}
                    color={category.color}>
                    {category.name}
                  </Tag>
                </div>
              </div>
            </div>
          )}
        </CardElement>
      </div>
    </EditPage>
  );
}
