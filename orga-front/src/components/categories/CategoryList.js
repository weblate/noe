import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {ListPage} from "../common/ListPage";
import {listSorter} from "../../helpers/listUtilities";
import {ColorDot} from "./CategoryEdit";
import {useTranslation} from "react-i18next";

export function CategoryList({navigate}) {
  const {t} = useTranslation();
  const categories = useSelector(categoriesSelectors.selectList);
  const dispatch = useDispatch();

  const columns = [
    {
      title: t("categories:schema.color.label"),
      dataIndex: "color",
      width: "80pt",
      render: (text, record) => <ColorDot color={record.color} />,
    },
    {
      title: t("common:schema.name.label"),
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
  ];

  useEffect(() => {
    dispatch(categoriesActions.loadList());
  }, []);

  return (
    <ListPage
      i18nNs="categories"
      elementsActions={categoriesActions}
      navigateFn={navigate}
      columns={columns}
      dataSource={categories}
      groupEditable
      groupImportable
    />
  );
}
