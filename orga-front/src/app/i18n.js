import i18next from "i18next";
import {initReactI18next} from "react-i18next";
import BrowserLanguageDetector from "i18next-browser-languagedetector";
import resources from "./locales";

export const initI18n = () =>
  i18next
    .use(BrowserLanguageDetector)
    .use(initReactI18next)
    .init({
      resources,
      defaultNS: "common",
      fallbackNS: "common",
      debug: process.env !== "production",
      interpolation: {escapeValue: false},
      fallbackLng: "fr",
    });
