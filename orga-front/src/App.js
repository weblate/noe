import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "./features/currentUser";
import {LogIn} from "./components/common/login/LogIn.js";
import {SignUp} from "./components/common/login/SignUp.js";
import {ForgotPassword} from "./components/common/login/ForgotPassword.js";
import {ResetPassword} from "./components/common/login/ResetPassword.js";
import {MainLayout} from "./components/MainLayout.js";
import {ProjectLayout} from "./components/ProjectLayout.js";
import {Redirect, Router} from "@reach/router";

import "./style/App.less";
import "./style/ant.css";
import "./style/containers.css";

import {ProjectNew} from "./components/projects/ProjectNew";
import {ProjectList} from "./components/projects/ProjectList";
import {displayStagingInfoNotification} from "./components/common/LayoutElement";
import isHotkey from "is-hotkey";
import {useTranslation} from "react-i18next";
const isCtrlSavePressed = isHotkey("mod+S");

function App() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const connected = useSelector(currentUserSelectors.selectConnected);
  const user = useSelector(currentUserSelectors.selectUser);

  // Remove Ctrl+S default behavior
  const onCtrlSKeyDown = (event) => isCtrlSavePressed(event) && event.preventDefault();
  useEffect(() => {
    window.addEventListener("keydown", onCtrlSKeyDown);
    return () => {
      window.removeEventListener("keydown", onCtrlSKeyDown);
    };
  }, []);

  const userCanCreateProject =
    user.superAdmin || process.env.REACT_APP_BLOCK_PROJECT_CREATION !== "true";

  useEffect(() => {
    if (connected === undefined) {
      dispatch(currentUserActions.refreshAuthTokens());
    }
  }, [connected]);

  displayStagingInfoNotification();

  return (
    <Router>
      {connected === false && (
        <>
          {/*Simple root URLs first*/}
          <LogIn path="/login" subtitle={t("common:connectionPage.orgaLogIn")} />
          <SignUp path="/signup" />
          <ForgotPassword path="/forgotpassword" />
          <ResetPassword path="/resetpassword" />

          {/*With project ID URLs after*/}
          <LogIn path="/:envId/login" subtitle={t("common:connectionPage.orgaLogIn")} />
          <SignUp path="/:envId/signup" />
          <ForgotPassword path="/:envId/forgotpassword" />

          {/*Redirect /projects to /login so it is not considered as a project slug*/}
          <Redirect noThrow from="/projects" to="/login" />

          {/*Redirect to login page if user is lost*/}
          <Redirect noThrow from="/:envId/*" to="/:envId/login" />
          <Redirect noThrow from="/*" to="/login" />
        </>
      )}
      {connected && (
        <>
          {/*If the user is on the /login page, redirect to /projects*/}
          <Redirect noThrow from="/login" to="/projects" />

          {/*Main Layout*/}
          <MainLayout path="/projects" page="projects">
            <ProjectList path="/" userCanCreateProject={userCanCreateProject} />
          </MainLayout>

          {/*Access to project creation page*/}
          {userCanCreateProject && (
            <MainLayout path="/new">
              <ProjectNew path="/" id="new" />
            </MainLayout>
          )}

          {/*Compatibility with old "/projects/:envId" URls */}
          <Redirect noThrow from="/projects/:envId/*" to="/:envId" />

          {/*Project view*/}
          <ProjectLayout path="/:envId/*" />

          <Redirect noThrow from="/*" to="/projects" />
        </>
      )}
    </Router>
  );
}

export default App;
