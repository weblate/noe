import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Input, List} from "antd";
import {LayoutElement} from "./LayoutElement";
import {useDebounce, useSavedPagination, useWindowDimensions} from "../../helpers/viewUtilities";
import {paginationPageSizes} from "../../features/view";
import {useTranslation} from "react-i18next";

const {Search} = Input;

export function GridPage({
  title,
  subtitle,
  buttonTitle,
  renderItem,
  header = true,
  fullPage = true,
  searchItems,
  dataSource,
  navigateFn,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination(dispatch, title);
  const {isMobileView} = useWindowDimensions();
  const [searchText, setSearchText] = useState("");

  const filteredDataSource =
    searchText !== "" ? dataSource.filter((item) => searchItems(item, searchText)) : dataSource;

  const debouncedSearch = useDebounce(setSearchText, 500);

  return (
    <>
      <div
        className={`containerV ${fullPage ? "full-width-content" : ""}`}
        style={{background: "#fafafa", position: "sticky", top: 0, zIndex: 4}}>
        {header && (
          <LayoutElement.PageHeading
            className="list-page-header"
            title={title}
            buttonTitle={buttonTitle}
            onButtonClick={() => navigateFn("new")}
          />
        )}
        {(subtitle || searchItems) && (
          <div className="subtitle with-margins" style={{marginBottom: 20}}>
            {subtitle}
            {searchItems && (
              <Search
                placeholder={t("common:search")}
                style={{minWidth: 200, flexGrow: 4, flexBasis: 3}}
                onChange={(e) => debouncedSearch(e.target.value)}
                enterButton
              />
            )}
          </div>
        )}
      </div>
      <List
        style={{paddingTop: 10, marginBottom: 40, background: "white"}}
        grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 4}}
        dataSource={filteredDataSource}
        renderItem={renderItem}
        pagination={{
          position: "bottom",
          style: {textAlign: "center"},
          size: isMobileView && "small",
          showSizeChanger: true,
          onChange: setPagination,
          current: pagination.current,
          pageSize: pagination.pageSize,
          pageSizeOptions: paginationPageSizes,
        }}
      />
    </>
  );
}
