import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, message, Space} from "antd";

import {TableElement} from "../common/TableElement";
import Search from "antd/es/input/Search";

import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {viewSelectors} from "../../features/view";
import {personName} from "../../helpers/utilities";
import {Trans, useTranslation} from "react-i18next";

export function RegistrationTicketing({currentProject, saveDataFn, infoMessage}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {ticketId} = useSelector(viewSelectors.selectSearchParams);

  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentRegistrationTickets = currentRegistration[`${currentProject.ticketingMode}Tickets`];
  const isHelloAssoTicketing = currentProject.ticketingMode === "helloAsso";
  const helloAssoUrl =
    isHelloAssoTicketing &&
    `https://www.helloasso.com/associations/${currentProject.helloAsso.organizationSlug}/evenements/${currentProject.helloAsso.selectedEvent}`;

  // Automagically add the ticketId if there is one
  useEffect(() => {
    if (
      ticketId &&
      !currentRegistrationTickets?.find((ticket) => ticket.id === parseInt(ticketId)) // Make sure we don't add the ticket twice
    )
      addTicket(ticketId);
  }, [ticketId]);

  const columnsTickets = [
    {
      title: t("registrations:ticketing.schema.id"),
      dataIndex: "id",
    },
    isHelloAssoTicketing && {
      title: t("registrations:ticketing.schema.userName"),
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: t("registrations:ticketing.schema.article"),
      dataIndex: "name",
    },
    {
      title: t("registrations:ticketing.schema.amount"),
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
  ].filter((el) => !!el);

  const addTicket = (ticketId) => dispatch(registrationsActions.addTicketToRegistration(ticketId));

  const removeTicket = (record) => {
    // If there is only one ticket and user has booked, then we can't
    if (currentRegistration.inDatabase.ticketingIsOk && currentRegistrationTickets.length <= 1) {
      message.error(t("registrations:ticketing.messages.unregisterBeforeRemovingTicket"));
    } else {
      // Else if there are many or if the user is not booked, just delete it and update the modif button
      dispatch(registrationsActions.removeTicketFromRegistration(record.id));
    }
  };

  return (
    <div className="containerV" style={{marginBottom: 50}}>
      <h3 className="list-element-header">
        {currentRegistration?.ticketingIsOk
          ? t("registrations:ticketing.labelMyTickets")
          : t("registrations:ticketing.labelIHaveATicket")}
      </h3>
      {infoMessage}
      {isHelloAssoTicketing && !currentRegistration?.ticketingIsOk && (
        <Alert
          style={{marginBottom: 26}}
          message={t("registrations:ticketing.noHelloAssoTicketAlert.message")}
          description={
            //  TODO check
            <Trans
              i18nKey="ticketing.noHelloAssoTicketAlert.description"
              ns="registrations"
              components={{
                linkToHelloAsso: (
                  <a href={helloAssoUrl} onClick={saveDataFn} target="_blank" rel="noreferrer" />
                ),
              }}
            />
          }
        />
      )}
      {currentRegistration?.ticketingIsOk && (
        <TableElement.Simple
          showHeader
          style={{marginBottom: 15}}
          columns={columnsTickets}
          onDelete={removeTicket}
          rowKey="id"
          dataSource={currentRegistrationTickets}
        />
      )}

      <Space wrap>
        <strong>
          {currentRegistration?.ticketingIsOk
            ? t("registrations:ticketing.addAnotherTicket")
            : t("registrations:ticketing.addYourTicketHere")}
        </strong>
        <Search
          allowClear
          style={{maxWidth: 250}}
          enterButton="Ajouter"
          onSearch={addTicket}
          placeholder={t("registrations:ticketing.ticketInputPlaceholder")}
        />
      </Space>
    </div>
  );
}
