# Contribuer au projet NOÉ

<!-- TODO: 
- Comment nous contacter pour faire partie de l'équipe, etc
- Les profils qu'on cherche (pas que tech !)
-->

Nous vous invitons à vous reporter au [fichier `CONTRIBUTING.md`](https://gitlab.com/alternatiba/noe/-/blob/master/CONTRIBUTING.md#d%C3%A9ployer-lapplication) disponible sur notre dépôt GitLab.
