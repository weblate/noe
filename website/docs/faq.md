# FAQ / Troubleshooting

:::caution Contenu obsolète

Le contenu de cette FAQ était destiné aux orgas de Camps Climat en 2021 mais n'a pas été mise à jour depuis. La majorité
du contenu y est obsolète mais nous la gardons à titre indicatif.

:::

:movie_camera: Si tu cherches la **nouvelle vidéo de présentation**,
c'est [par là](https://peertube.virtual-assembly.org/videos/watch/37c55e47-428c-497f-b454-28a16de73041?start=4m28s).

:movie_camera: Si tu cherches les **anciennes** vidéos tutoriel,
c'est [par ici](https://video.liberta.vip/video-channels/noe/videos).

:book: Si tu cherches de la **documentation sur le fonctionnement de l'appli**, va
plutôt [là](https://docs.google.com/document/d/1ZrXxMbOHF33W7KvD9xfXApIMGWHAQddLo6kxR1570f4/edit#).

## La démarche pour résoudre tous tes soucis

### Lis la [FAQ](#FAQ) juste en dessous

Elle regroupe les interrogations souvent rencontrées par les utilisateur·ices et pourra t'être d'une grande aide.

### Fais appel à ta référent.e locale sur l'application NOE

Dans chaque groupe local utilisant l'application NOE, il y a un·e référent·e de l'application. Iel a peut-être la
réponse à ta question.

Le ou la référent·e locale pourra alors solliciter le réseau des référent·es NOE via
le [groupe Telegram dédié à ce sujet](https://t.me/joinchat/W35RbY--hU42OWU0).

Le mieux est alors de mentionner tous les éléments dont tu disposes : ce que tu as essayé de faire, ce qui a marché, pas
marché, quel a été le résultat, etc. Ces détails sont très utiles pour que l'équipe NOE comprenne d'où vient le
problème.

## FAQ

Voici quelques réponses aux questions récurrentes que l'on reçoit, tu y trouveras peut-être ton bonheur ;-)

### Page d'accueil

#### Je n'arrive pas à uploader une image sur la page d'accueil

On ne peut pas charger directement d'image de son disque dur. Il faut :

- Mettre en ligne l'image sur un hébergeur comme https://pic.infini.fr/
- Copier le lien de l'image en vérifiant qu'il termine bien par ".png", ".jpg" etc.
- Dans l'édition de la page d'accueil, choisir l'ajout par lien et coller ce lien

#### Le texte de la page d'accueil est collé à la barre de gauche c'est moche.

:::caution Réponse obsolète
Cette réponse fonctionne mais il y a plus facile ! 
:::

Il est possible d'ajouter des marges à droite et à gauche :

- Dans l'éditeur de page d'accueil, choisir "Add things" et ajouter un "Spacer"
- Glisser le plugin "spacer" à gauche (puis à droite) du texte, une barre noire indique où il sera ajouté et dans quel
  orientation.
- Une fois les 2 spacers ajoutés, réduire leur taille au maximum en utilisant l'outil "resize things"

### Suivi des inscriptions

#### On dirait qu'il manque des inscrit.e.s sur mon évènement...

:::caution Réponse obsolète
Depuis, le fonctionnement de NOÉ avec Hello Asso est différent et améliore l'expérience des utilisateur·ices sur l'application.
:::

Une erreur récurrente que font les participant.e.s est de s'arrêter dès qu'on a notre billet HelloAsso. Or ce n'est qu'
une confirmation de paiement, pas d'inscription ! Il reste à reporter le numéro de billet et valider.

Il est donc conseillé d'**aller voir de temps en temps le nombre de billets achetés sur HelloAsso pour comparer au
nombre d'inscrits sur NOE**. En cas de différence, pour éviter de comparer manuellement 1 à 1 les listes de HelloAsso et
NOE, vous pouvez utiliser une formule Excel comme montré
sur [cet exemple](https://docs.google.com/spreadsheets/d/1yHH-qp97hID4AZntbcpMBCMa2EyFWOq7j1MIPkNiHSc/edit#gid=0)

Penser aussi à entrer un message de confirmation adapté sur HelloAsso


#### Comment faire si un.e participant.e a payé plusieurs billets d'un coup sur HelloAsso ?

:::caution Réponse obsolète
Depuis, le fonctionnement de NOÉ avec Hello Asso est différent et améliore l'expérience des utilisateur·ices sur l'application.
:::

C'est tout à fait possible car HelloAsso délivre un numéro de billet par place achetée.
Il faut suivre la démarche suivante :

- Une fois les billets achetés, n'en renseigner qu'un seul dans l'inscription en cours. Si plusieurs ont été renseignés,
  supprimer des billets en trop et enregistrer pour valider la suppression
- Recommencer l'inscription avec un nouveau compte (création de compte, dates et formulaire)
- Entrer directement l'autre numéro de billet (reçu par mail, en téléchargeant le billet ou directement dans le mail,
  après "Référence:"). Pas besoin de reouvrir HelloAsso
- Enregistrer l'inscription

#### Un·e participant·e ne parvient pas à utiliser l'interface ou a des difficultés, puis-je le faire à sa place ?

:::info Réponse incomplète
Il y a plus facile (on peut inscrire les participant.es aux sessions directement depuis l'espace orga désormais + modifier leur formulaire d'inscription direct depuis l'interface orga également)!
:::

Oui, à condition que la personne soit inscrite l'aide de la fonctionnalité "Changer d'identité" (actuellement visible
dans l'interface participant.e seulement).
