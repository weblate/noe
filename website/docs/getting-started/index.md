---
sidebar_position: 2
---

# Premiers pas

Créez votre premier événement avec NOÉ !

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```