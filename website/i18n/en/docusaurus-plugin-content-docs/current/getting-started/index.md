---
sidebar_position: 2
---

# Getting started

Create your first event with NOÉ !

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```