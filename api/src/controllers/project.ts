import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {
  readEntityHistory,
  lightSelection,
  readEntity,
  updateManyToManyRelationship,
  listAllEntities,
  computeFunctionForAllElements,
} from "../utilities/controllersUtilities";
import {handleErrors} from "../services/errors";

import {Project} from "../models/projects";
import {FormMapping, Registration} from "../models/registrations";
import {Activity, ActivityD} from "../models/activities";
import {Place, PlaceD} from "../models/places";
import {Category, CategoryD} from "../models/categories";
import {Slot, SlotD} from "../models/slots";
import {Steward, StewardD} from "../models/stewards";
import {Session, SessionD} from "../models/sessions";
import {
  emailNormalizationParams,
  nobodyExceptSuperAdmins,
  permit,
  projectAdmins,
  projectContributors,
  projectGuests,
  ROLES,
  validateAndSanitizeBody,
} from "../utilities/permissionsUtilities";
import {Team, TeamD} from "../models/teams";
import {User} from "../models/users";
import {body} from "express-validator/check";
import moment = require("moment");
import config from "../config/config";
import {logger} from "../services/logger";
import {personName} from "../utilities/utilities";

export class ProjectController {
  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  static async getAllEntitiesLinkedToProject(projectId: string, withRegistrations = false) {
    // find all entities linked to this project and store them in promises
    const query = {project: projectId} as any;
    const [activities, categories, places, sessions, stewards, teams] = await Promise.all([
      Activity.find(query),
      Category.find(query),
      Place.find(query),
      Session.find(query).populate("slots"),
      Steward.find(query),
      Team.find(query),
    ]);

    const registrations = withRegistrations
      ? await Registration.find(query).populate("user")
      : undefined;

    return {activities, categories, places, sessions, stewards, teams, registrations};
  }

  async create(req: Request, res: Response) {
    const project = new Project({
      name: req.body.name,
      availabilitySlots: req.body.availabilitySlots,
      start: req.body.start,
      end: req.body.end,
      useTeams: req.body.useTeams,
      usePlaces: req.body.usePlaces,
      isPublic: req.body.isPublic,
    });
    await project.save();

    // Create the project registration linked to this project, and make the current user admin of the project
    const userRegistration = new Registration({
      role: "admin",
      project: project,
      user: req.user,
    });
    await userRegistration.save();

    // Prepare the project for response
    await project.hideSensitiveFields(userRegistration);
    res.status(201).json(project);
  }

  async read(req: Request, res: Response) {
    const registration = req.user.registration;
    const role = registration?.role; // Maybe there is no registration at all !

    return await readEntity(
      req.params.projectId,
      undefined,
      res,
      (id) => Project.findByIdOrSlug(id),
      async (project: any) => {
        if (role === ROLES.ADMIN && project.ticketingMode === "helloAsso") {
          await project.refreshHelloAssoAuthToken();
          await project.populateHelloAssoEventCandidates();
        }
        await project.hideSensitiveFields(registration);
        return project;
      }
    );
  }

  async readPublic(req: Request, res: Response) {
    return await readEntity(req.params.projectId, undefined, res, (id) =>
      Project.findByIdOrSlug(id, {name: 1, content: 1, openingState: 1, full: 1, theme: 1}).lean()
    );
  }

  async getAllData(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const withRegistrations = req.query.withRegistrations === "true";
    const project = await Project.findByIdOrSlug(projectId);
    if (!project) return res.status(404).end();

    res.status(200).json({
      ...(await ProjectController.getAllEntitiesLinkedToProject(projectId, withRegistrations)),
      project,
    });
  }

  async replaceSessions(req: Request, res: Response) {
    // TODO Make a really better check for the sake of everybody
    // Check if the request really comes from the IA backend
    if (req.headers.origin !== config.urls.iaBack) return res.send(401);

    const projectId: string = req.params.projectId;

    // Delete the current program //

    const notScheduleFrozenSessionsIds = await Session.find({
      project: projectId,
      isScheduleFrozen: false,
    } as any);

    const sessionsToDeleteIds = notScheduleFrozenSessionsIds.map((session) => session._id);
    const slotsToDeleteIds = notScheduleFrozenSessionsIds
      .map((session) => session.slots) // Get the slots
      .reduce((accSlots, slot) => accSlots.concat(slot), []) // Flatten
      .map((slot) => slot._id); // Get the slots ids

    logger.info(`-------- IA: Replacing the sessions on project ${projectId}. --------`);
    logger.info("Sessions to delete:", sessionsToDeleteIds);
    logger.info("Slots to delete:", slotsToDeleteIds);

    await Session.updateMany(
      {_id: sessionsToDeleteIds, project: projectId} as any, // stay in the current project only
      {deletedAt: new Date()}
    );

    await Slot.updateMany(
      {_id: slotsToDeleteIds}, // delete the slots belonging to those sessions that are not frozen
      {deletedAt: new Date()}
    );

    // Write the new program //

    for (const session of req.body) {
      const startSlot = moment(session.start);
      session.project = projectId;
      const activity = await Activity.findById(session.activity);

      const slots: any[] = [];
      for (const slotActivity of activity.slots) {
        const start = startSlot.toDate();
        startSlot.add(slotActivity.duration, "minute");
        const slot = {
          start: start,
          end: startSlot.toDate(),
          stewards: session.stewards,
          places: [session.places],
          duration: slotActivity.duration,
          stewardsSessionSynchro: true,
          placesSessionSynchro: true,
        };

        const insertedSlot = await Slot.create(slot);
        slots.push(insertedSlot._id);
      }
      session.slots = slots;
      await Session.create(session);
    }
    res.status(200).json(req.body);
  }

  async update(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    // First, know who the person is (there can be no registration at all)
    const userRegistration = req.user.registration;
    const userIsAdmin = userRegistration?.role === ROLES.ADMIN;

    const project = await Project.findByIdOrSlug(projectId);
    if (!project) return res.status(404).end();

    const shouldUpdateAllRegistrationsDaysOfPresence =
      (req.body.availabilitySlots &&
        JSON.stringify(project.availabilitySlots.map((s) => s.toObject())) !==
          JSON.stringify(req.body.availabilitySlots)) ||
      (req.body?.breakfastTime && project.breakfastTime.toISOString() !== req.body.breakfastTime) ||
      (req.body?.lunchTime && project.lunchTime.toISOString() !== req.body.lunchTime) ||
      (req.body?.dinnerTime && project.dinnerTime.toISOString() !== req.body.dinnerTime);

    project.name = req.body.name;
    project.formComponents = req.body.formComponents;
    // Remove all the form mappings that are not complete
    project.formMapping = req.body.formMapping.filter(
      (mapping: FormMapping) => mapping?.formField && mapping?.columnName?.length > 0
    );
    project.content = req.body.content;
    project.minMaxVolunteering = req.body.minMaxVolunteering;
    project.blockVolunteeringUnsubscribeIfBeginsSoon =
      req.body.blockVolunteeringUnsubscribeIfBeginsSoon;
    project.start = req.body.start;
    project.end = req.body.end;
    project.openingState = req.body.openingState;
    project.isPublic = req.body.isPublic;
    project.blockSubscriptions = req.body.blockSubscriptions;
    project.notAllowOverlap = req.body.notAllowOverlap;
    project.notes = req.body.notes;
    project.full = req.body.full;
    project.secretSchedule = req.body.secretSchedule;

    project.availabilitySlots = req.body.availabilitySlots;
    project.lunchTime = req.body.lunchTime;
    project.lunchTime = req.body.lunchTime;
    project.dinnerTime = req.body.dinnerTime;

    // Update admin-only fields
    if (userIsAdmin) {
      // Ticketing mode
      project.ticketingMode = req.body.ticketingMode;

      // Ticketing - Custom API
      const shouldUpdateCustomTicketing = project.customTicketing !== req.body.customTicketing;
      if (shouldUpdateCustomTicketing) {
        project.customTicketing = req.body.customTicketing;
      }

      // Ticketing - HelloAsso
      const shouldUpdateHelloAssoTicketing =
        project.helloAsso?.clientId !== req.body.helloAsso?.clientId ||
        project.helloAsso?.clientSecret !== req.body.helloAsso?.clientSecret ||
        project.helloAsso?.selectedEvent !== req.body.helloAsso?.selectedEvent;
      if (shouldUpdateHelloAssoTicketing) {
        if (
          req.body.helloAsso?.clientId?.length > 0 &&
          req.body.helloAsso?.clientSecret?.length > 0
        ) {
          project.helloAsso = req.body.helloAsso;
          await project.getHelloAssoAuthTokens();
          await project.populateHelloAssoEventCandidates();
        } else {
          project.helloAsso = {
            clientId:
              req.body.helloAsso?.clientId?.length > 0 ? req.body.helloAsso?.clientId : undefined,
            clientSecret:
              req.body.helloAsso?.clientId?.length > 0
                ? req.body.helloAsso?.clientSecret
                : undefined,
          };
        }
      }

      // Ticketing - TiBillet
      const shouldUpdateTiBilletTicketing =
        project.tiBillet?.serverUrl !== req.body.tiBillet?.serverUrl ||
        project.tiBillet?.eventSlug !== req.body.tiBillet?.eventSlug ||
        project.tiBillet?.apiKey !== req.body.tiBillet?.apiKey;
      if (shouldUpdateTiBilletTicketing) {
        project.tiBillet = req.body.tiBillet;
      }

      // Features
      project.usePlaces = req.body.usePlaces;
      project.useTeams = req.body.useTeams;
      project.useAI = req.body.useAI;
    }

    await project.save({__reason: "update()", __user: req.authenticatedUser._id} as any);
    await project.hideSensitiveFields(userRegistration);

    res.status(200).json(project);

    // Compute all days of presence of all registrations if the project dates or meal times change
    if (shouldUpdateAllRegistrationsDaysOfPresence) {
      // Update asynchronously all dates of presence if it is needed
      computeFunctionForAllElements(
        await Registration.find({project: projectId} as any, "availabilitySlots project"),
        async (el) => {
          await el.computeDaysOfPresence();
          await el.save({
            __reason: "update() days of presence",
            __user: req.authenticatedUser._id,
          } as any);
        }
      ).then(() => logger.info("Successfully updated dates of presence of all registrations."));
    }
  }

  async listAllPublic(req: Request, res: Response) {
    return listAllEntities(
      Project.find(
        {isPublic: true},
        {...lightSelection.project, content: 1, openingState: 1, full: 1}
      ),
      {start: "asc"},
      res
    );
  }

  async listAll(req: Request, res: Response) {
    const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;
    return isRequestedFromInscriptionFrontend
      ? await this.listAllProjectsForParticipant(req, res)
      : await this.listAllProjectsForOrga(req, res);
  }

  async listAllProjectsForOrga(req: Request, res: Response) {
    const registeredProjectsIds = await Registration.distinct("project", {
      user: req.user,
      role: {$exists: true},
    });

    // Fetch the projects administrated by the user. If super admin, show alllll the projects of the platform

    return listAllEntities(
      Project.find(
        // Fetch the projects administrated by the user. If super admin, show alllll the projects of the platform
        req.user.superAdmin ? {} : {_id: registeredProjectsIds},
        lightSelection.project
      ),
      {updatedAt: "desc"},
      res
    );
  }

  async listAllProjectsForParticipant(req: Request, res: Response) {
    const registeredProjectsIds = await Registration.distinct("project", {
      user: req.user,
    });

    return listAllEntities(
      Project.find({_id: registeredProjectsIds}, {...lightSelection.project, content: 1}),
      {start: "asc"},
      res
    );
  }

  async delete(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    const entitiesDictionary = await ProjectController.getAllEntitiesLinkedToProject(
      projectId,
      true
    );

    const entities: any[] = [project];
    Object.entries(entitiesDictionary).forEach(([, value]) => {
      value.forEach((entity: any) => {
        entities.push(entity);
      });
    });

    // Delete entities including project
    await Promise.all(
      entities.map(async (entity) => {
        entity.deletedAt = new Date();
        await entity.save();
      })
    );
    res.status(200).end();
  }

  async createNewElements(
    importedElements: Array<any>,
    createNewElementCallback: (importedElement: any) => any
  ) {
    const newElements: Array<any> = [];
    for (const importedElement of importedElements) {
      const newElement = createNewElementCallback(importedElement);
      await newElement.save();
      newElements.push(newElement);
    }
    return newElements;
  }

  async deleteExistingElements(
    klass: any,
    existingProjectId: any,
    beforeDeleteAction?: (oldElements: any) => any
  ) {
    const oldElements = await klass.find({
      $and: [{project: existingProjectId}, {deletedAt: {$exists: false}}],
    });
    beforeDeleteAction && beforeDeleteAction(oldElements);
    for (const oldElement of oldElements) {
      oldElement.deletedAt = new Date();
      await oldElement.save({validateBeforeSave: false});
    }
  }

  prepareImportedData(importedElements: Array<any>, existingProjectId?: any) {
    return (
      importedElements?.map((el: any) => ({
        ...el,
        importedId: el._id,
        _id: undefined,
        project: existingProjectId,
      })) || []
    );
  }

  // Return the id, or undefined if the importedID is null
  getByImportedId(list: Array<any>, importedId: string): string {
    return importedId ? list.find((el) => el.importedId === importedId)._id : undefined;
  }

  async import(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const additiveImportForElements = req.query.additiveImport === "true";
    const withRegistrations = req.query.withRegistrations === "true";
    const existingProject = await Project.findByIdOrSlug(projectId);
    if (!existingProject) return res.status(404).end();

    const importedProject = req.body;

    // Extract data from the imported project
    let {
      activities: importedActivities,
      stewards: importedStewards,
      sessions: importedSessions,
      places: importedPlaces,
      categories: importedCategories,
      teams: importedTeams,
      registrations: importedRegistrations,
      project: importedProjectData,
      // eslint-disable-next-line prefer-const
      ...theRest
    } = importedProject;

    // TODO LEGACY: code built to support previous export files
    // Files that had a difference arborescence (no 'project' object, but everything in the root object)
    if (importedProjectData === undefined) {
      importedProjectData = theRest;
    }
    // Legacy domains
    if (importedCategories === undefined) {
      importedCategories = importedProject.domains;
    }
    // TODO LEGACY end of code

    // Give each imported element an importedId, and link to the existing project + remove the _id so it can be recreated by Mongoose

    importedActivities = this.prepareImportedData(importedActivities, existingProject._id);
    importedStewards = this.prepareImportedData(importedStewards, existingProject._id);
    importedSessions = this.prepareImportedData(importedSessions, existingProject._id);
    importedPlaces = this.prepareImportedData(importedPlaces, existingProject._id);
    importedCategories = this.prepareImportedData(importedCategories, existingProject._id);
    importedTeams = this.prepareImportedData(importedTeams, existingProject._id);

    // If it's an additive import, don't delete the elements
    if (!additiveImportForElements) {
      await Promise.all([
        this.deleteExistingElements(Place, existingProject._id),
        this.deleteExistingElements(Category, existingProject._id),
        this.deleteExistingElements(Steward, existingProject._id),
        this.deleteExistingElements(Activity, existingProject._id),
        this.deleteExistingElements(Slot, existingProject._id),
        // For teams and sessions, we need to unsubscribe people before deleting them
        this.deleteExistingElements(Session, existingProject._id, (oldSessions) =>
          oldSessions?.forEach((el: SessionD) =>
            updateManyToManyRelationship(el, [], Registration, "session", "sessionsSubscriptions")
          )
        ),
        this.deleteExistingElements(Team, existingProject._id, (oldTeams) =>
          oldTeams?.forEach((el: TeamD) =>
            updateManyToManyRelationship(el, [], Registration, "team", "teamsSubscriptions")
          )
        ),
      ]);
    }

    const newPlaces: Array<PlaceD> = await this.createNewElements(
      importedPlaces,
      (importedEl) => new Place({...importedEl})
    );
    const newCategories: Array<CategoryD> = await this.createNewElements(
      importedCategories,
      (importedEl) => new Category({...importedEl})
    );
    const newStewards: Array<StewardD> = await this.createNewElements(
      importedStewards,
      (importedEl) => new Steward({...importedEl})
    );
    const newActivities: Array<ActivityD> = await this.createNewElements(
      importedActivities,
      (importedActivity) => {
        return new Activity({
          ...importedActivity,
          category: this.getByImportedId(newCategories, importedActivity.category),
          stewards: importedActivity.stewards?.map((el: string) =>
            this.getByImportedId(newStewards, el)
          ),
          places: importedActivity.places?.map((el: string) => this.getByImportedId(newPlaces, el)),
        });
      }
    );

    const newTeams: Array<TeamD> = await this.createNewElements(importedTeams, (importedTeam) => {
      return new Team({
        ...importedTeam,
        activity: this.getByImportedId(newActivities, importedTeam.activity),
      });
    });

    const newSessions: any[] = [];
    for (const session of importedSessions) {
      const sessionSlots: any = await this.prepareImportedData(session.slots, existingProject._id);
      const newSlots: Array<SlotD> = await this.createNewElements(
        sessionSlots,
        (slot) =>
          new Slot({
            ...slot,
            stewards: slot.stewards?.map((el: string) => this.getByImportedId(newStewards, el)),
            places: slot.places?.map((el: string) => this.getByImportedId(newPlaces, el)),
          })
      );

      const newSession = new Session({
        ...session,
        slots: newSlots.map((s: any) => s._id),
        activity: this.getByImportedId(newActivities, session.activity),
        stewards: session.stewards.map((el: string) => this.getByImportedId(newStewards, el)),
        places: session.places.map((el: string) => this.getByImportedId(newPlaces, el)),
        // TODO allow a session to be used by multiple teams ?
        team: this.getByImportedId(newTeams, session.team),
      });
      await newSession.save();
      newSessions.push(newSession);
    }

    if (withRegistrations) {
      // Set imported Ids for registration objects
      importedRegistrations = this.prepareImportedData(importedRegistrations, existingProject._id);

      for (const importedRegistration of importedRegistrations) {
        // TODO: right now, the info about the subscribedBy is not properly reimported if the user doesn't exist yet /!\

        // Replace the source ID
        importedRegistration.user.importedId = importedRegistration.user._id;
        delete importedRegistration.user._id;

        // First check if the user exists and create if it doesn't
        const existingUser = await User.findOne({email: importedRegistration.user.email});
        const user = existingUser || (await User.create(importedRegistration.user));

        // Now that we created the user, replace it with the id in the registration
        importedRegistration.user = user._id;

        // If this registration exists already and has already a registration, don't import it.
        // Else, import it in hidden mode
        const existingRegistration = await Registration.findOne({
          user: importedRegistration.user,
          project: existingProject._id,
        });
        if (!existingRegistration) {
          const teamsSubscriptions = importedRegistration.teamsSubscriptions.map(
            (teamSubscription: any) => ({
              ...teamSubscription,
              team: this.getByImportedId(newTeams, teamSubscription.team),
            })
          );
          const sessionsSubscriptions = importedRegistration.sessionsSubscriptions.map(
            (sessionSubscription: any) => ({
              ...sessionSubscription,
              session: this.getByImportedId(newSessions, sessionSubscription.session),
              team: this.getByImportedId(newTeams, sessionSubscription.team),
            })
          );
          await Registration.create({
            ...importedRegistration,
            hidden: true,
            sessionsSubscriptions,
            teamsSubscriptions,
          });

          if (!existingUser) {
            logger.debug(
              `Created new user ${personName(user)} without password + related hidden registration`
            );
          } else {
            logger.debug(
              `Created related hidden registration for existing user ${personName(user)}`
            );
          }
        } else {
          logger.debug(
            `Existing registration found for existing user ${personName(
              user
            )}. Not modifying anything.`
          );
        }
      }
    }

    // Keep the name of the project intact
    importedProjectData.name = existingProject.name;

    // Assign the imported project config data by replacing it when it exists in the importedProjectData
    Object.assign(existingProject, importedProjectData);
    await existingProject.save({__reason: "import()", __user: req.authenticatedUser._id} as any);

    res.status(200).json(existingProject);
  }

  async rollbackDeletedElements(klass: any, existingProjectId: any, startDate: Date) {
    const deletedElementsToRollback = await klass.find({
      searchAlsoInDeletedElements: true,
      $and: [{project: existingProjectId}, {deletedAt: {$gte: startDate}}],
    });
    for (const deletedElement of deletedElementsToRollback) {
      logger.debug(`Rolled back element ${deletedElement._id}`);
      deletedElement.deletedAt = undefined;
      try {
        await deletedElement.save({validateBeforeSave: false});
      } catch (e) {
        console.error(e);
      }
    }
  }

  async rollback(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const rollbackPeriod: number = parseInt(req.params.rollbackPeriod);

    const rollbackStartDate = new Date();
    rollbackStartDate.setMinutes(rollbackStartDate.getMinutes() - rollbackPeriod); // Remove the period given as argument
    await Promise.all([
      this.rollbackDeletedElements(Place, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Category, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Steward, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Session, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Slot, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Activity, projectId, rollbackStartDate),
      this.rollbackDeletedElements(Team, projectId, rollbackStartDate),
    ]);

    return res.status(200).end();
  }

  /**
   * Get a project history
   * GET /:projectId/history
   */
  async readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.projectId, req.params.projectId, "Project", res);
  }

  setupRoutes() {
    this.router.get("/", userLoggedIn, handleErrors(this.listAll.bind(this)));
    this.router.get("/public", handleErrors(this.listAllPublic.bind(this)));
    this.router.post(
      "/",
      // Allow project creation only for super  admins, or for everybody
      config.blockProjectsCreation ? [userLoggedIn, permit(nobodyExceptSuperAdmins)] : userLoggedIn,
      handleErrors(this.create)
    );
    this.router.get(
      "/:projectId",
      userLoggedIn,
      permit(projectGuests, config.urls.inscriptionFront), // Allow all persons having a role in orga front, and any person requesting from the inscription side
      handleErrors(this.read)
    );
    this.router.get("/:projectId/public", handleErrors(this.readPublic));
    this.router.get(
      "/:projectId/allData",
      userLoggedIn,
      permit(projectContributors),
      handleErrors(this.getAllData)
    );
    this.router.post(
      "/:projectId/import",
      userLoggedIn,
      permit(projectAdmins),
      validateAndSanitizeBody(
        body("registrations.*.user.email").isEmail().normalizeEmail(emailNormalizationParams)
      ),
      handleErrors(this.import.bind(this))
    );
    this.router.get(
      "/:projectId/rollback/:rollbackPeriod",
      userLoggedIn,
      permit(projectAdmins),
      handleErrors(this.rollback.bind(this))
    );
    this.router.post("/:projectId/replaceSessions", handleErrors(this.replaceSessions));
    this.router.patch(
      "/:projectId",
      userLoggedIn,
      permit(projectContributors),
      handleErrors(this.update)
    );
    this.router.delete(
      "/:projectId",
      userLoggedIn,
      permit(projectAdmins),
      handleErrors(this.delete)
    );
    this.router.get(
      "/:projectId/history",
      userLoggedIn,
      permit(projectGuests),
      handleErrors(this.readHistory)
    );
  }
}

const projectController = new ProjectController();
const ProjectRouter = projectController.router;

export {ProjectRouter};
