import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  getDependenciesList,
  readEntityHistory,
  lightSelection,
  readEntity,
  removeNestedObjectsContent,
  updateEntity,
  listAllEntities,
} from "../utilities/controllersUtilities";
import {
  permit,
  projectContributors,
  projectAdmins,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activities";
import {Session} from "../models/sessions";
import {Place, allowedBodyArgs, schemaDescription} from "../models/places";
import {Slot} from "../models/slots";

export const PlaceRouter = Router({mergeParams: true});

/**
 * Get the place schema definition
 * GET /schema
 */
PlaceRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the places in the project
 * GET /
 */
PlaceRouter.get(
  "/",
  userLoggedIn,
  handleErrors(async function listAll(req: Request, res: Response) {
    return listAllEntities(
      Place.find({project: req.params.projectId} as any, lightSelection.place),
      {name: "asc"},
      res
    );
  })
);

/**
 * Get a place
 * GET /:id
 */
PlaceRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Place.findOne({_id: id, project: projectId} as any).lean()
    );
  })
);

/**
 * Create a place
 * POST /
 */
PlaceRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("project"),
  handleErrors(async function create(req: Request, res: Response) {
    return await createEntity(Place, req.body, res);
  })
);

/**
 * Modify a place
 * PATCH /:id
 */
PlaceRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("project"),
  handleErrors(async function update(req: Request, res: Response) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Place,
      req.body,
      req.authenticatedUser,
      res
    );
  })
);

/**
 * Delete a place
 * DELETE /:id
 */
PlaceRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const placeId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {places: placeId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    const dependentSessionsList = await getDependenciesList(Session, {places: placeId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    const dependentSlotsList = await getDependenciesList(
      Slot,
      {places: placeId},
      async (e: any) => {
        await e.populate("session").execPopulate();
        return e.session._id;
      }
    );
    if (dependentSlotsList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des plages de sessions (${dependentSlotsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(placeId, Place, req.authenticatedUser, res);
  })
);

/**
 * Get a place history
 * GET /:id/history
 */
PlaceRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Place", res);
  })
);
