import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  isValidObjectId,
  lightSelection,
} from "../utilities/controllersUtilities";
import {
  emailNormalizationParams,
  filterBody,
  permit,
  projectAdmins,
  validateAndSanitizeBody,
} from "../utilities/permissionsUtilities";
import {body} from "express-validator/check";

import {User, allowedBodyArgs} from "../models/users";
import {compare as bcryptCompare} from "bcryptjs";
import {Registration} from "../models/registrations";

const UserRouter = Router();

/**
 * Get a user's basic details in database
 * GET /:id (can be either email or _id)
 */
UserRouter.get(
  "/:id",
  userLoggedIn,
  permit(projectAdmins), // Only admins can add new people to projects
  handleErrors(async function get(req: Request, res: Response) {
    const userId = req.params.id;
    // Match either with ID or with email
    if (req.user._id === userId || req.user.email === userId) {
      return res.status(200).send(req.user);
    } else {
      const conditions = isValidObjectId(userId)
        ? {$or: [{_id: userId}, {email: userId}]}
        : {email: userId};
      const user = await User.findOne(
        // Make sure we only select full, completely defined users (not users from invitations)
        {
          $and: [
            conditions,
            {firstName: {$exists: true}, lastName: {$exists: true}, password: {$exists: true}},
          ],
        },
        lightSelection.user
      ).lean();
      if (!user) return res.status(404).end();
      return res.status(200).send(user);
    }
  })
);

/**
 * Create a new user
 * POST /
 */
UserRouter.post(
  "/",
  filterBody(allowedBodyArgs),
  validateAndSanitizeBody(
    body("email").trim().isEmail().normalizeEmail(emailNormalizationParams),
    body(["firstName", "lastName"]).trim().isLength({min: 1}),
    body("password").isLength({min: 8}),
    body("locale").isString().trim().isLength({min: 2, max: 2}).optional()
  ),
  handleErrors(async function create(req: Request, res: Response) {
    const submittedUser = req.body;

    // Check if there is already a user in the DB with this email
    const user = await User.findOne({email: submittedUser.email}, "_id password");

    if (user) {
      // If the user already exists, check that the user in DB doesn't have password (it is an invited user)

      // If the user already has a password, it means we can't update it, because a real user has already created an account with this email
      if (user.password) return res.sendStatus(401);

      // Then, update details
      user.password = submittedUser.password;
      user.firstName = submittedUser.firstName;
      user.lastName = submittedUser.lastName;
      await user.save();

      // And remove all invitation tokens from all other pending invitations for this user.
      // This means that all invitations will be accepted automatically
      await Registration.updateMany({user: user._id}, {$unset: {invitationToken: true}});

      // Code 201 to tell it was updated, not created
      return res.status(201).json(user);
    } else {
      // If there is not existing user, just create one
      return await createEntity(User, req.body, res);
    }
  })
);

/**
 * Modify a user
 * PATCH /:id
 */
UserRouter.patch(
  "/:id",
  userLoggedIn,
  filterBody(["_id", ...allowedBodyArgs]),
  validateAndSanitizeBody(
    body("email").trim().isEmail().normalizeEmail(emailNormalizationParams).optional(),
    body(["firstName", "lastName"]).trim().isLength({min: 1}).optional(),
    body("password").isLength({min: 8}).optional(),
    body("locale").isString().trim().isLength({min: 2, max: 2}).optional()
  ),
  handleErrors(async function update(req: Request, res: Response) {
    const authenticatedUser = req.authenticatedUser;
    const {password, oldPassword, ...payload} = req.body;

    // Make sure we modify the authenticated user profile, otherwise do not allow allow it
    if (authenticatedUser._id.toString() !== req.params.id) return res.status(401).end();

    // If there is a password change, make the proper checks
    if (password && oldPassword) {
      const user = await User.findById(req.body._id, {
        password: true,
      });
      const passwordsAreEqual = await bcryptCompare(oldPassword, user.password);
      if (!passwordsAreEqual) return res.status(401).send("Mot de passe incorrect");
      user.password = req.body.password;
      user.save();
    }

    // Update non critical stuff
    const user = await User.findByIdAndUpdate(req.body._id, payload, {new: true});

    return res.status(200).json(user);
  })
);

/**
 * Delete a user
 * DELETE /:id
 */
UserRouter.delete(
  "/:id",
  userLoggedIn,
  handleErrors(async function del(req: Request, res: Response) {
    const userId = req.params.id;
    const authenticatedUser = req.authenticatedUser;

    // Make sure we modify the authenticated user profile
    if (authenticatedUser._id.toString() !== userId) return res.status(401).end();

    // Delete *for real* the user
    return await deleteEntity(
      userId,
      User,
      req.authenticatedUser,
      res,
      // Delete *for real* all the registrations linked to the user
      async () => await Registration.deleteMany({user: userId} as any),
      undefined,
      true
    );
  })
);

export {UserRouter};
