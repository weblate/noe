import {Request} from "express";
import {createTransport} from "nodemailer";
import {logger} from "./logger";
import config from "../config/config";
import {compileHtmlTemplate} from "./html-and-pdf";
import {t} from "i18next";

const md = require("markdown").markdown;

const SMTP_CONFIG =
  config.smtp.server && config.smtp.address && config.smtp.pass
    ? `smtps://${config.smtp.address}:${config.smtp.pass}@${config.smtp.server}/?pool=true`
    : undefined;

const transporter = SMTP_CONFIG && createTransport(SMTP_CONFIG);

const headerImageUrl = `${config.urls.api}/assets/images/logo-colors-with-blue-text.png`;

class Mailer {
  async sendMail(emailKey: string, to: string, i18nOptions: any, req: Request): Promise<void> {
    // Generate the body
    const text = t(`emails:${emailKey}.body`, i18nOptions);
    const html = await compileHtmlTemplate("emails", {
      headerImageUrl,
      emailBody: md.toHTML(text),
    });

    // Assemble parameters
    const emailParams = {
      subject: t(`emails:${emailKey}.subject`, i18nOptions),
      to,
      from: `${config.instanceName} <${config.smtp.address || "fake@smtp.test"}>`, // Fallback on fake@smtp.test if there is no proper config
      ...i18nOptions,
    };

    // Send the email
    if (SMTP_CONFIG && config.env === "production") {
      // If we are not in test mode, and that we have a correct SMTP config, use the real SMTP provider
      await transporter.sendMail({...emailParams, text, html});
      logger.info(`Email '${emailKey}' sent:`, emailParams);
    } else {
      // If we are in test mode, or if we don't have a correct SMTP config, use the fake one
      logger.info(`[FAKE SMTP] Fake email '${emailKey}' sent:`, {
        ...emailParams,
        text,
        html,
      });
      // If in production with no correct config, throw a warning
      config.env === "production" &&
        logger.warn("[FAKE SMTP] No valid SMTP config is given, so emails are not sent for real");
    }
  }
}

export default new Mailer();
