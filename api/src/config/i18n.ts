import config from "./config";
import i18next from "i18next";
import Backend from "i18next-fs-backend";
import i18nextMiddleware from "i18next-http-middleware";
import {Express} from "express";

export const initI18n = (app: Express) => {
  i18next
    .use(i18nextMiddleware.LanguageDetector)
    .use(Backend)
    .init({
      // debug: true,
      backend: {
        loadPath: `./${config.localesPath}/{{lng}}/{{ns}}.yml`,
        addPath: `./${config.localesPath}/{{lng}}/{{ns}}.missing.yml`,
      },
      fallbackLng: "fr",
      ns: [
        "activities",
        "categories",
        "cockpit",
        "common",
        "emails",
        "places",
        "projects",
        "registrations",
        "sessions",
        "stewards",
        "teams",
        "users",
        "welcome",
      ],
      defaultNS: "common",
      preload: ["en", "fr"],
    });

  // Add the middleware to the Express app
  app.use(i18nextMiddleware.handle(i18next));
};
